Source: coq-corn
Maintainer: Debian OCaml Maintainers <debian-ocaml-maint@lists.debian.org>
Uploaders: Julien Puydt <jpuydt@debian.org>
Section: ocaml
Priority: optional
Standards-Version: 4.6.1
Rules-Requires-Root: no
Build-Depends: coq,
               debhelper-compat (= 13),
               dh-coq,
               dh-ocaml,
               libcoq-bignums,
               libcoq-math-classes,
               libcoq-ocaml-dev,
               libcoq-stdlib
Vcs-Browser: https://salsa.debian.org/ocaml-team/coq-corn
Vcs-Git: https://salsa.debian.org/ocaml-team/coq-corn.git
Homepage: https://github.com/coq-community/corn

Package: libcoq-corn
Architecture: any
Depends: ${coq:Depends}, ${misc:Depends}
Provides: ${coq:Provides}
Description: Coq Constructive Repository at Nijmegen
 This library provides different theories for Coq:
  - an algebraic hierarchy with an axiomatic formalization
 of the most common algebraic structures, like setoids,
 monoids, groups, rings, fields, ordered fields, rings of
 polynomials and real and complex numbers;
  - a construction of the real numbers satisfying the above
 axiomatic description;
  - a proof of the fundamental theorem of algebra;
  - a collection of elementary results on real analysis
  including continuity, differentiability, integration,
  Taylor's theorems and the fundamental theorem of calculus;
  - tools for exact real computations like real numbers,
  functions, integrals, graph of functions and differential
  equations.
 .
 Coq is a proof assistant for higher-order logic.
