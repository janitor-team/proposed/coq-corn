Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/

Files: *
Copyright: 1998-2022 Evgeny Makarov
	   1998-2022 Robbert Krebbers
	   1998-2022 Eelis van der Weegen
	   1998-2022 Bas Spitters
	   1998-2022 Jelle Herold
	   1998-2022 Russell O'Connor
	   1998-2022 Cezary Kaliszyk
	   1998-2022 Dan Synek
	   1998-2022 Luís Cruz-Filipe
	   1998-2022 Milad Niqui
	   1998-2022 Iris Loeb
	   1998-2022 Herman Geuvers
	   1998-2022 Randy Pollack
	   1998-2022 Freek Wiedijk
	   1998-2022 Jan Zwanenburg
	   1998-2022 Dimitri Hendriks
	   1998-2022 Henk Barendregt
	   1998-2022 Mariusz Giero
	   1998-2022 Rik van Ginneken
	   1998-2022 Dimitri Hendriks
	   1998-2022 Sébastien Hinderer
	   1998-2022 Bart Kirkels
	   1998-2022 Pierre Letouzey
	   1998-2022 Lionel Mamane
	   1998-2022 Nickolay Shmyrev
	   1998-2022 Vincent Semeria
License: GPL-2

Files: algebra/CAbGroups.v
       algebra/CAbMonoids.v
       algebra/CFields.v
       algebra/CGroups.v
       algebra/CMonoids.v
       algebra/COrdAbs.v
       algebra/COrdCauchy.v
       algebra/COrdFields.v
       algebra/COrdFields2.v
       algebra/CPoly_ApZero.v
       algebra/CPoly_Degree.v
       algebra/CPoly_NthCoeff.v
       algebra/CPolynomials.v
       algebra/CRing_Homomorphisms.v
       algebra/CRings.v
       algebra/CSemiGroups.v
       algebra/CSetoidFun.v
       algebra/CSetoidInc.v
       algebra/CSetoids.v
       algebra/CSums.v
       algebra/Cauchy_COF.v
       algebra/Expon.v
       complex/AbsCC.v
       complex/CComplex.v
       complex/Complex_Exponential.v
       complex/NRootCC.v
       coq_reals/Rreals.v
       coq_reals/Rreals_iso.v
       fta/CC_Props.v
       fta/CPoly_Contin1.v
       fta/CPoly_Rev.v
       fta/CPoly_Shift.v
       fta/FTA.v
       fta/FTAreg.v
       fta/KeyLemma.v
       fta/KneserLemma.v
       fta/MainLemma.v
       ftc/COrdLemmas.v
       ftc/CalculusTheorems.v
       ftc/Composition.v
       ftc/Continuity.v
       ftc/Derivative.v
       ftc/DerivativeOps.v
       ftc/Differentiability.v
       ftc/FTC.v
       ftc/FunctSequence.v
       ftc/FunctSeries.v
       ftc/FunctSums.v
       ftc/Integral.v
       ftc/IntervalFunct.v
       ftc/MoreFunSeries.v
       ftc/MoreFunctions.v
       ftc/MoreIntegrals.v
       ftc/MoreIntervals.v
       ftc/NthDerivative.v
       ftc/PartFunEquality.v
       ftc/PartInterval.v
       ftc/Partitions.v
       ftc/RefLemma.v
       ftc/RefSepRef.v
       ftc/RefSeparated.v
       ftc/RefSeparating.v
       ftc/Rolle.v
       ftc/StrongIVT.v
       ftc/Taylor.v
       ftc/TaylorLemma.v
       ftc/WeakIVT.v
       logic/CLogic.v
       logic/CornBasics.v
       logic/PropDecid.v
       metrics/CMetricSpaces.v
       metrics/CPMSTheory.v
       metrics/CPseudoMSpaces.v
       metrics/ContFunctions.v
       metrics/Equiv.v
       metrics/IR_CPMSpace.v
       metrics/LipExt.v
       metrics/Prod_Sub.v
       old/Transparent_algebra.v
       opaque/Opaque_algebra.v
       reals/Bridges_LUB.v
       reals/Bridges_iso.v
       reals/CMetricFields.v
       reals/CPoly_Contin.v
       reals/CReals.v
       reals/CReals1.v
       reals/CSumsReals.v
       reals/CauchySeq.v
       reals/Cauchy_CReals.v
       reals/Cesaro.v
       reals/IVT.v
       reals/Intervals.v
       reals/Max_AbsIR.v
       reals/NRootIR.v
       reals/OddPolyRootIR.v
       reals/PosSeq.v
       reals/Q_dense.v
       reals/Q_in_CReals.v
       reals/R_morphism.v
       reals/RealCount.v
       reals/RealFuncts.v
       reals/RealLists.v
       reals/Series.v
       reals/iso_CReals.v
       tactics/AlgReflection.v
       tactics/CornTac.v
       tactics/DiffTactics1.v
       tactics/DiffTactics2.v
       tactics/DiffTactics3.v
       tactics/FieldReflection.v
       tactics/Rational.v
       tactics/RingReflection.v
       tactics/Step.v
       tactics/csetoid_rewrite.v
       transc/ArTanH.v
       transc/Exponential.v
       transc/InvTrigonom.v
       transc/Pi.v
       transc/PowerSeries.v
       transc/RealPowers.v
       transc/SinCos.v
       transc/TaylorSeries.v
       transc/TrigMon.v
       transc/Trigonometric.v
       model/Zmod/Cmod.v
       model/Zmod/IrrCrit.v
       model/Zmod/ZBasics.v
       model/Zmod/ZDivides.v
       model/Zmod/ZGcd.v
       model/Zmod/ZMod.v
       model/Zmod/Zm.v
       model/abgroups/QSposabgroup.v
       model/abgroups/Qabgroup.v
       model/abgroups/Qposabgroup.v
       model/abgroups/Zabgroup.v
       model/fields/Qfield.v
       model/groups/QSposgroup.v
       model/groups/Qgroup.v
       model/groups/Qposgroup.v
       model/groups/Zgroup.v
       model/monoids/Nm_to_cycm.v
       model/monoids/Nm_to_freem.v
       model/monoids/Nmonoid.v
       model/monoids/Nposmonoid.v
       model/monoids/QSposmonoid.v
       model/monoids/Qmonoid.v
       model/monoids/Qposmonoid.v
       model/monoids/Zmonoid.v
       model/monoids/freem_to_Nm.v
       model/ordfields/Qordfield.v
       model/reals/Cauchy_IR.v
       model/rings/Qring.v
       model/rings/Zring.v
       model/semigroups/Npossemigroup.v
       model/semigroups/Nsemigroup.v
       model/semigroups/QSpossemigroup.v
       model/semigroups/Qpossemigroup.v
       model/semigroups/Qsemigroup.v
       model/semigroups/Zsemigroup.v
       model/setoids/Nfinsetoid.v
       model/setoids/Npossetoid.v
       model/setoids/Nsetoid.v
       model/setoids/Qpossetoid.v
       model/setoids/Qsetoid.v
       model/setoids/Zfinsetoid.v
       model/setoids/Zsetoid.v
       model/structures/Npossec.v
       model/structures/Nsec.v
       model/structures/Qpossec.v
       model/structures/Qsec.v
       model/structures/Zsec.v
Copyright: 1998-2022 Evgeny Makarov
	   1998-2022 Robbert Krebbers
	   1998-2022 Eelis van der Weegen
	   1998-2022 Bas Spitters
	   1998-2022 Jelle Herold
	   1998-2022 Russell O'Connor
	   1998-2022 Cezary Kaliszyk
	   1998-2022 Dan Synek
	   1998-2022 Luís Cruz-Filipe
	   1998-2022 Milad Niqui
	   1998-2022 Iris Loeb
	   1998-2022 Herman Geuvers
	   1998-2022 Randy Pollack
	   1998-2022 Freek Wiedijk
	   1998-2022 Jan Zwanenburg
	   1998-2022 Dimitri Hendriks
	   1998-2022 Henk Barendregt
	   1998-2022 Mariusz Giero
	   1998-2022 Rik van Ginneken
	   1998-2022 Dimitri Hendriks
	   1998-2022 Sébastien Hinderer
	   1998-2022 Bart Kirkels
	   1998-2022 Pierre Letouzey
	   1998-2022 Lionel Mamane
	   1998-2022 Nickolay Shmyrev
	   1998-2022 Vincent Semeria
License: GPL-2+

Files: reals/stdlib/CMTDirac.v
       reals/stdlib/CMTFullSets.v
       reals/stdlib/CMTIntegrableFunctions.v
       reals/stdlib/CMTIntegrableSets.v
       reals/stdlib/CMTMeasurableFunctions.v
       reals/stdlib/CMTPositivity.v
       reals/stdlib/CMTProductIntegral.v
       reals/stdlib/CMTReals.v
       reals/stdlib/CMTbase.v
       reals/stdlib/CMTcast.v
       reals/stdlib/CMTprofile.v
       reals/stdlib/ConstructiveCauchyIntegral.v
       reals/stdlib/ConstructiveDiagonal.v
       reals/stdlib/ConstructiveFastReals.v
       reals/stdlib/ConstructiveFasterReals.v
       reals/stdlib/ConstructivePartialFunctions.v
       reals/stdlib/ConstructiveUniformCont.v
       reals/stdlib/Markov.v
Copyright: 2020 Vincent Semeria
License: Expat

Files: debian/*
Copyright: 2022 Julien Puydt
License: GPL-2

License: Expat
 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation files
 (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge,
 publish, distribute, sublicense, and/or sell copies of the Software,
 and to permit persons to whom the Software is furnished to do so,
 subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

License: GPL-2
 This package is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation version 2.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian GNU/Linux systems, the complete text of the GNU General Public
 License version 2 can be found in `/usr/share/common-licenses/GPL-2'

License: GPL-2+
 This package is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either
 version 2 of the License or later.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian GNU/Linux systems, the complete text of the GNU General Public
 License version 2 can be found in `/usr/share/common-licenses/GPL-2'
